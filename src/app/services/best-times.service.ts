import { Injectable, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class BestTimesService {
  constructor(protected localStorage: LocalStorage) {
    this.localStorage.getItem('times')
      .subscribe((times: number[]) => {
        this.times = times || [];
        this._sortedTimesSubject.next(this.sortedTimeArray);
      });
  }

  times: number[] = [];
  _sortedTimesSubject = new BehaviorSubject<number[]>([]);

  get sortedTimes(): Observable<number[]> {
    return this._sortedTimesSubject;
  }

  addTime(time: number) {
    this.times.push(time);
    this.times = this.sortedTimeArray.slice(0, 10); // Keep only the top 10
    this.localStorage.setItem('times', this.times).subscribe(() => {});
    this._sortedTimesSubject.next(this.sortedTimeArray);
  }

  private get sortedTimeArray(){
    return this.times.sort((a, b) => a - b);
  }
}
