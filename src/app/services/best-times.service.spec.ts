import { TestBed } from '@angular/core/testing';

import { BestTimesService } from './best-times.service';

describe('BestTimesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BestTimesService = TestBed.get(BestTimesService);
    expect(service).toBeTruthy();
  });
});
