import { Map } from './../model/map';
import { LevelService } from './../level.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'level-chooser',
  templateUrl: './level-chooser.component.html',
  styleUrls: ['./level-chooser.component.scss']
})
export class LevelChooserComponent implements OnInit {

  levels: Map[] = [];

  @Input()
  level: Map = null;

  @Output()
  levelChange = new EventEmitter<Map>();

  constructor(private levelService: LevelService) { }

  ngOnInit(): void {
    this.levelService.levels$.subscribe(lvls => {
      this.levels = lvls;
    });
  }

  onChange($event): void {
    this.levelChange.emit(this.level);
  }

}
