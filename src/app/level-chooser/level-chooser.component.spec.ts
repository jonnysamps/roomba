import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevelChooserComponent } from './level-chooser.component';

describe('LevelChooserComponent', () => {
  let component: LevelChooserComponent;
  let fixture: ComponentFixture<LevelChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevelChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
