import { BestTimesService } from './services/best-times.service';
import { Component, HostListener, OnInit } from '@angular/core';
import { Map } from './model/map';
import { AppearanceType, Tile, MovementType } from './model/tile';
import { LevelService } from './level.service';

const STRAIGHT_FACE = '😐';
const HURT_FACE = '🤕';
const TIRED_FACE = '😩';
const ANGRY_FACE = '😡';
const HAPPY_FACE = '😁';
const DEAD_FACE = '😵';
const WIN_FACE = '😎';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(private bestTimes: BestTimesService, private levelService: LevelService){}

  get map(): Map {
    return this.levels[this.levelIndex];
  }
  set map(v: Map) {
    this.levelIndex = this.levels.indexOf(v);
  }
  editorMode = true;
  title = 'roomba';
  levels: Map[] = [];
  levelIndex = 0;

  playerAtX = 0;
  playerAtY = 0;
  playerState = STRAIGHT_FACE;

  wallHits = 0;

  debug = true;

  dialogMessage = null;
  dialogSubMessage = null;
  timeStart: number = null;
  timeNow: number = null;
  timeInterval = null;

  restingTimeout;

  ngOnInit(): void {
    this.levelService.levels$.subscribe(levels => this.levels = levels);
    this.reset();
  }

  @HostListener('document:keydown', ['$event'])
  handleDeleteKeyboardEvent(event: KeyboardEvent): void {
    if (this.playerState === DEAD_FACE) {
      this.reset();
      return;
    }
    if (this.playerState === WIN_FACE) {
      this.levelIndex = this.levelIndex === this.levels.length - 1 ? 0 : this.levelIndex + 1;
      this.reset();
      return;
    }

    let newX = this.playerAtX;
    let newY = this.playerAtY;
    if (event.key === 'ArrowRight') {
      newX = this.playerAtX + 1;
      event.preventDefault();
    } else if (event.key === 'ArrowLeft') {
      newX = this.playerAtX - 1 ;
      event.preventDefault();
    } else if (event.key === 'ArrowDown') {
      newY = this.playerAtY + 1;
      event.preventDefault();
    } else if (event.key === 'ArrowUp') {
      newY = this.playerAtY - 1;
      event.preventDefault();
    } else if (event.key === '0') {
      this.editorMode = true;
      return;
    } else {
      return;
    }


    if (this.playerAtX !== newX || this.playerAtY !== newY){
      this.tryMove(newX, newY);
    }
  }

  tryMove(newX: number, newY: number): void {
    if (this.timeStart === null){
      this.timeNow = this.timeStart = Date.now().valueOf();
      this.timeInterval = setInterval(() => this.timeNow = Date.now().valueOf(), 100);
    }

    let moved = false;
    const newRow = this.map.grid[newY];
    let newTile: Tile;
    if (newRow){
      newTile = newRow[newX];
      if (newTile && newTile.moveType !== MovementType.obstacle){
        this.playerAtX = newX;
        this.playerAtY = newY;
        moved = true;
      }
    }

    if (moved){
      if (newTile.moveType === MovementType.finish){
        this.win();
      } else if (newTile.moveType === MovementType.death){
        this.lose();
      } else {
        // this.playerState = TIRED_FACE;
        // this.setRestingTimeout();
      }
    } else {
      this.wallHits++;
      if (this.playerState === HURT_FACE){
        this.playerState = ANGRY_FACE;
      } else {
        this.playerState = HURT_FACE;
      }

      if (this.wallHits >= 3){
        this.lose();
      }
    }
  }

  lose(): void {
    clearTimeout(this.restingTimeout);
    clearInterval(this.timeInterval);
    this.playerState = DEAD_FACE;
    this.dialogMessage = 'YOU DEAD';
    this.dialogSubMessage = '(Press any key to try again)';
    this.reset();
  }

  win(): void {
    clearTimeout(this.restingTimeout);
    clearInterval(this.timeInterval);
    this.playerState = WIN_FACE;
    this.dialogMessage = this.map.name + ' CLEARED!';
    this.dialogSubMessage = '(Press any key to move to the next level)';
    this.bestTimes.addTime(this.timeNow - this.timeStart);
  }

  reset(): void {
    this.playerAtX = this.map.start.x;
    this.playerAtY = this.map.start.y;
    this.dialogMessage = null;
    this.playerState = STRAIGHT_FACE;
    this.wallHits = 0;
    this.timeStart = null;
  }

  time(): number {
    return (this.timeNow - this.timeStart) / 1000;
  }
  setRestingTimeout(): void {
    clearTimeout(this.restingTimeout);
    this.restingTimeout = setTimeout(() => {
      switch (this.wallHits){
        case 0: this.playerState = STRAIGHT_FACE; break;
        case 1: this.playerState = HURT_FACE;     break;
        case 2: this.playerState = ANGRY_FACE;    break;
      }

      this.restingTimeout = null;
    }, 500);
  }

  newMap(): void {
    const newMap = new Map('New Level',
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    '
    ,
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    \n' +
    '                    '
    );
    this.levelService.add(newMap);
    this.map = newMap;
    this.editorMode = true;
  }
}
