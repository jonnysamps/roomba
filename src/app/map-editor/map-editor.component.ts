import { MovementType } from './../model/tile';
import { Component, OnInit, Input } from '@angular/core';
import { Map } from '../model/map';
import { AppearanceType } from '../model/tile';
import { ClipboardService } from 'ngx-clipboard';
import { LevelService } from '../level.service';

@Component({
  selector: 'map-editor',
  templateUrl: './map-editor.component.html',
  styleUrls: ['./map-editor.component.scss']
})
export class MapEditorComponent {

  @Input()
  map: Map = new Map('New Level',
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    '
  ,
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    \n' +
  '                    '
  );
  constructor(private levelService: LevelService) { }


  tileClicked({x, y}: {x: number, y: number}): void {
    const tile = this.map.grid[y][x];
    switch (tile.appearType) {
      case AppearanceType.path:
        tile.appearType = AppearanceType.wall;
        break;
      case AppearanceType.wall:
        tile.appearType = AppearanceType.grass;
        break;
      case AppearanceType.grass:
        tile.appearType = AppearanceType.hole;
        break;
      case AppearanceType.hole:
        tile.appearType = AppearanceType.desert;
        break;
      case AppearanceType.desert:
        tile.appearType = AppearanceType.path;
        break;
    }
    this.levelService.save();
  }

  tileRightClicked({x, y}: {x: number, y: number}): void {
    const tile = this.map.grid[y][x];
    switch (tile.moveType) {
      case MovementType.path:
        tile.moveType = MovementType.obstacle;
        break;
      case MovementType.obstacle:
        tile.moveType = MovementType.death;
        break;
      case MovementType.death:
        tile.moveType = MovementType.start;
        break;
      case MovementType.start:
        tile.moveType = MovementType.finish;
        break;
      case MovementType.finish:
        tile.moveType = MovementType.path;
        break;
    }
    this.levelService.save();
  }

}
