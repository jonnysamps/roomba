import { map } from 'rxjs/operators';
import { Map } from './model/map';
import { Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { levels } from './levels';

const STORE_KEY_LEVELS = 'roomba_levels';

@Injectable({
  providedIn: 'root'
})
export class LevelService {

  levels: BehaviorSubject<Map[]>;
  constructor() {
    let storedLevels = JSON.parse(localStorage.getItem(STORE_KEY_LEVELS));
    if (storedLevels) {
      storedLevels = storedLevels.map(m => Map.build(m));
    }
    const lvls = storedLevels ? storedLevels : levels;
    this.levels = new BehaviorSubject<Map[]>(lvls);
  }

  get levels$(): Observable<Map[]>{
    return this.levels.asObservable();
  }

  add(m: Map): void {
    const maps = this.levels.value;
    maps.push(m);
    this.levels.next(maps);
  }

  save(): void {
    localStorage.setItem(STORE_KEY_LEVELS, JSON.stringify(this.levels.value));
  }
}
