import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Map } from '../model/map';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent {
  @Input()
  map: Map;

  @Input()
  playerAtX: number;

  @Input()
  playerAtY: number;

  @Input()
  playerState: string;

  @Output()
  tileClick = new EventEmitter();

  @Output()
  tileRightClick = new EventEmitter();

  @Input()
  showMovementType = false;

  debug = false;

  constructor() { }

  tileClicked(tile): boolean {
    this.tileClick.emit(tile);
    return false;
  }

  tileRightClicked(tile): boolean {
    this.tileRightClick.emit(tile);
    return false;
  }

}
