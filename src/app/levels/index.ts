import { level3 } from './level3';
import { level1 } from './level1';
import { level2 } from './level2';

export const levels = [
    level1,
    level2,
    level3
];
