import { Map } from '../model/map';

export const level3 = new Map('Knox\'s Level',
'wwwuw\n'+
'uwwww\n'+
'uwuuw\n'+  
'uwuuw\n'+  
'uwuuw\n'+  
'uwuuw\n'+  
'uwuuw\n'+ 
'uwuuw\n'+  
'uwuww\n'+  
'uwuwu\n'+  
'uwuww\n'+  
'uwuww\n'+ 
'uuwwu\n'+  
'swwww'    
,
's  x \n'+
'x    \n'+
'x xx \n'+  
'x xx \n'+  
'x xx \n'+  
'x xx \n'+  
'x xx \n'+ 
'x xx \n'+  
'x x  \n'+  
'x x x\n'+  
'x x  \n'+  
'x x  \n'+ 
'xx  x\n'+  
'f    '    
);