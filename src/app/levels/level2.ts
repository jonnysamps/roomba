import { Map } from '../model/map';

export const level2 = new Map('Level 2',
'||||||||||||||||||||\n'+
'|          |    |  s\n'+
'||u|u|u|u|   |     |\n'+  
'|u u u u u uuuuuuuu|\n'+  
'|         |        |\n'+
'||u|u|u u|u u|u u u|\n'+
'|           |   |  |\n'+
'|u|u|u u|u|u|u|u|u |\n'+
'|     |            |\n'+
'| u u u|u|u|u |uuuu|\n'+
'|  |       ||     u|\n'+  
'|u u u|u|u u u u u |\n'+  
'|  | ||| | | |     |\n'+  
'w  |     |         |\n'+  
'||||||||||||||||||||'    
,
'00000000000000000000\n'+
'0 0        0    0  f\n'+
'00 0 0x000   0     0\n'+  
'0x x x x x xxx xxxx0\n'+  
'0         0        0\n'+
'00x0x0x x0x x0x x x0\n'+
'0           0   0  0\n'+
'0x0x0x x0x0x0x0x0x 0\n'+
'0     0            0\n'+
'0 x x x0x0x0x 0xxxx0\n'+
'0  0       00     x0\n'+  
'0x x x0x0x x x x x 0\n'+  
'0  0 000 0 0 0     0\n'+  
's  0     0         0\n'+  
'00000000000000000000'    
);