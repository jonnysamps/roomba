import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { BestTimesComponent } from './best-times/best-times.component';
import { MapEditorComponent } from './map-editor/map-editor.component';
import { ClipboardModule } from 'ngx-clipboard';
import { LevelChooserComponent } from './level-chooser/level-chooser.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    BestTimesComponent,
    MapEditorComponent,
    LevelChooserComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
