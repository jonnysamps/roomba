export class Tile {
    constructor(
        public appearType: AppearanceType, 
        public moveType: MovementType, 
        public items: ItemType[]){}
}

export enum AppearanceType {
    path = 'path',
    wall = 'wall',
    grass = 'grass',
    hole = 'hole',
    desert = 'desert'
}

export enum MovementType {
    path = 'path',
    death = 'death',
    start = 'start',
    finish = 'finish',
    obstacle = 'obstacle'
}

export enum ItemType {
    card = 'card',
    key = 'key'
}