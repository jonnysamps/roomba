import { Tile, AppearanceType, MovementType, ItemType } from './tile';

const CharToAppearenceType = {
  ' ': AppearanceType.path,
  '|': AppearanceType.wall,
  w: AppearanceType.grass,
  u: AppearanceType.hole,
  s: AppearanceType.desert
};

const CharToMoveType = {
  s: MovementType.start,
  f: MovementType.finish,
  x: MovementType.death,
  ' ': MovementType.path,
  0: MovementType.obstacle
};

const CharToItemType = {
  k: ItemType.key
};

export type Point = { x: number, y: number };
export class Map {

  start: Point = { x: 0, y: 0 };

  public grid: Array<Array<Tile>> = [];
  constructor(public name = '', private appearanceGridStr: string = '', private movementGridStr: string = '') {
    this.parseAppearenceGridString();
    this.parseMovementGridString();
  }

  public static build(obj: any): Map {
    const ret = new Map();
    return Object.assign(ret, obj);
  }

  private parseAppearenceGridString(): void {
    let row = new Array<Tile>();
    this.grid.push(row);
    for (let i = 0; i < this.appearanceGridStr.length; i++) {
      const char = this.appearanceGridStr.charAt(i);
      if (char === '\n') {
        row = new Array<Tile>();
        this.grid.push(row);
        continue;
      }
      let appearType = CharToAppearenceType[char];
      if (!appearType) { appearType = AppearanceType.path; }
      row.push(new Tile(appearType, MovementType.death, []));
    }
  }

  private parseMovementGridString(): void {
    let j = 0;
    let k = 0;
    for (let i = 0; i < this.movementGridStr.length; i++, k++) {
      const char = this.movementGridStr.charAt(i);
      if (char === '\n') {
        k = -1, j++;
        continue;
      }
      let moveType = CharToMoveType[char];
      if (!moveType) { moveType = MovementType.death; }
      this.grid[j][k].moveType = moveType;

      if (moveType === MovementType.start) {
        this.start = { x: k, y: j };
      }
    }
  }

  // TODO: implement
  // toJSON(): any {
  //   const result: any = {
  //     name: this.name,
  //     appearGridStr: '',
  //     moveGridStr: ''
  //   };

  //   return result;
  // }
}
