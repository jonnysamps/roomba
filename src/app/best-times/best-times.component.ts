import { BestTimesService } from './../services/best-times.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';''

@Component({
  selector: 'best-times',
  templateUrl: './best-times.component.html',
  styleUrls: ['./best-times.component.scss']
})
export class BestTimesComponent implements OnInit {

  constructor(private bestTimes: BestTimesService) { }

  times: Observable<number[]>;
  ngOnInit() {
    this.times = this.bestTimes.sortedTimes.pipe(
      map(t => {
        if(t.length < 10) 
          t.splice(t.length, 0, ...new Array(10-t.length))
        return t;
      })
    ); 
  }

}
